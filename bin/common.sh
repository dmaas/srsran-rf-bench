
SRCDIR=/var/tmp
CFGDIR=/local/repository/etc
OAI_RAN_MIRROR="https://gitlab.flux.utah.edu/powder-mirror/openairinterface5g"
SRS_REPO="https://github.com/srsran/srsRAN"
SRS_PROJECT_REPO="https://github.com/srsRAN/srsRAN_Project"
SRSGUI_REPO="https://github.com/srsran/srsGUI"
OPEN5GS_REPO="https://github.com/open5gs/open5gs"
